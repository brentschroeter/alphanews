# AlphaNews: Google Searches for News Articles in Julia

```julia-repl
julia> using AlphaNews

julia> search(Query("julia machine learning", Date(2019, 11, 1), Date(2019, 11, 30), 0))

10-element Array{Result,1}:
 Result("https://hub.packtpub.com/julia-computing-research-team-runs-machine-learning-model-on-encrypted-data-without-decrypting-it/", "Julia Computing research team runs machine learning ", 2019-11-28)
 Result("https://analyticsindiamag.com/is-julia-finally-catching-up-with-python-r/", "Is Julia Finally Catching Up With Python & R?", 2019-11-08)
 Result("https://devclass.com/2019/11/26/julia-1-3-gets-experimental-with-multi-threading/", "Julia 1.3 offers experimental approach to multi-threading", 2019-11-26)
 Result("https://insidebigdata.com/2019/11/10/julia-the-programming-language-of-the-future/", "Julia: The Programming Language Of The Future", 2019-11-10)
 Result("https://jaxenter.com/julia-1-3-164711.html", "Julia v1.3: Reproducible results, Yggdrasil, & multi-", 2019-11-27)
 Result("https://blogs.scientificamerican.com/observations/ai-doesnt-actually-exist-yet/", "AI Doesn't Actually Exist Yet", 2019-11-12)
 Result("https://www.datanami.com/2019/11/13/deep-learning-has-hit-a-wall-intels-rao-says/", "Deep Learning Has Hit a Wall, Intel's Rao Says", 2019-11-13)
 Result("https://www.datanami.com/2019/11/19/machine-learning-market-demonstrates-solid-growth/", "Machine Learning Market Demonstrates Solid Growth", 2019-11-19)
 Result("https://www.i-programmer.info/news/98-languages/13284-julia-improves-multithreading.html", "Julia Improves Multithreading", 2019-11-28)
 Result("https://sdtimes.com/softwaredev/sd-times-news-digest-amazon-announces-rekognition-custom-labs-scala-js-1-0-0-rc1-and-julia-1-3/", "news digest: Amazon announces Rekognition Custom ", 2019-11-26)
 ```
