module AlphaNews

export DEFAULT_HEADERS, Query, Result, search

using Cascadia
using Dates
using Gumbo
using HTTP
using HTTP.URIs

const DEFAULT_HEADERS = Dict(
    "Accept-Language" => "en-US,en;q=0.5",
    "User-Agent" =>
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:73.0) Gecko/20100101 Firefox/73.0",
)
const TAB_ID = "nws"

"""
    Query(search, start_date, end_date, offset)

Represents a set of parameters for a Google search of news articles.

# Examples

```julia-repl
julia> AlphaNews.Query("You stay classy, San Diego.", Date(2019, 1, 1), Date(2019, 2, 28), 0)
```
"""
struct Query
    search::String
    start_date::Union{Date, Nothing}
    end_date::Union{Date, Nothing}
    offset::Int
end

"""
    Result(url, title, publish_date)

Data for a Google search result in the News tab. `publish_date` might be `nothing`, for example if
the date information displayed is imprecise (e.g., "1 month ago").
"""
struct Result
    url::String
    title::String
    publish_date::Union{Date, Nothing}
end

"""
    search(q::Query, headers=DEFAULT_HEADERS)::Vector{Result}

Execute a Google search for news articles, defined by the query parameters in `q`.

# Examples

```julia-repl
julia> q = AlphaNews.Query("scotchy scotch scotch", Date(2019, 1, 1), Date(2019, 2, 28), 0)
julia> AlphaNews.search(q)
10-element Array{Result, 1}: ...
```
"""
function search(q::Query, headers::Dict{String, String}=DEFAULT_HEADERS)::Vector{Result}
    resp = HTTP.get(construct_url(q), headers)
    html = parsehtml(String(resp.body))
    results = Vector{Result}()
    for a in eachmatch(sel"a[style=\"text-decoration:none;display:block\"]", html.root)
        wrapper = a.children[1]
        content = wrapper.children[length(wrapper.children)]
        title = text(content.children[2][1])
        node = content.children[3].children[2]
        while isa(node, Gumbo.HTMLElement)
            node = node.children[length(node.children)]
        end
        publish_date = nothing
        try
            publish_date = Date(node.text, DateFormat("u d, yyyy"))
        catch err
            if !isa(err, ArgumentError)
                throw(err)
            end
        end
        push!(results, Result(getattr(a, "href"), title, publish_date))
    end
    return results
end

function construct_url(q::Query)::String
    uri_q = replace(q.search, r"\s" => "+")
    uri_tbs = "sbd:1,cdr:1"
    if q.start_date != nothing
        uri_tbs *= ",cd_min:$(Dates.format(q.start_date, "m/d/yyyy"))"
    end
    if q.end_date != nothing
        uri_tbs *= ",cd_max:$(Dates.format(q.end_date, "m/d/yyyy"))"
    end
    qs = "?q=$(escapeuri(uri_q))&tbm=$(TAB_ID)&tbs=$(escapeuri(uri_tbs))&start=$(q.offset)"
    return "https://www.google.com/search$(qs)"
end

end
